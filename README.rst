*************************
pspvis
*************************

Gist
==========

Source Code Repository
---------------------------

|source| `Repository <https://gitlab.com/pradyparanjpe/pspvis.git>`__

|pages| `Documentation <https://pradyparanjpe.gitlab.io/pspvis>`__

Badges
---------

|Pipeline|  |Coverage|  |PyPi Version|  |PyPi Format|  |PyPi Pyversion|


Description
==============

**P**\ CA-assisted **S**\ preadsheet **P**\ lot **Vis**\ ualization

What does it do
--------------------

Displays data from *.csv* file, with:

- 2D/3D axes
- Raw data/Transpose of data (rows & columns swapped)
- Truncated (curently: column 1,2,3; customizable: in future) / PCA of data

  - PCA of transpose of data

- Customizable Group Annotations


.. |Pipeline| image:: https://gitlab.com/pradyparanjpe/pspvis/badges/master/pipeline.svg

.. |source| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg
   :width: 50
   :target: https://gitlab.com/pradyparanjpe/pspvis.git

.. |pages| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-stacked-rgb.svg
   :width: 50
   :target: https://pradyparanjpe.gitlab.io/pspvis

.. |PyPi Version| image:: https://img.shields.io/pypi/v/pspvis
   :target: https://pypi.org/project/pspvis/
   :alt: PyPI - version

.. |PyPi Format| image:: https://img.shields.io/pypi/format/pspvis
   :target: https://pypi.org/project/pspvis/
   :alt: PyPI - format

.. |PyPi Pyversion| image:: https://img.shields.io/pypi/pyversions/pspvis
   :target: https://pypi.org/project/pspvis/
   :alt: PyPi - pyversion

.. |Coverage| image:: https://gitlab.com/pradyparanjpe/pspvis/badges/master/coverage.svg?skip_ignored=true
