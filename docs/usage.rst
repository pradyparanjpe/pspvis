#######
USAGE
#######

**********
SYNOPSIS
**********

.. argparse::
   :ref: pspvis.command_line._cli
   :prog: pspvis


********************
Launch GUI
********************

.. tabbed:: executable

   .. code-block:: sh

      $ pspvis


.. tabbed:: module-call

   .. code-block:: sh

      $ python3 -m pspvis


**************
Imports
**************

- import ``plot.py`` to use without the user-interface.
- import ``DataAnnot`` to programmatically compose annotation file.

**************
Instructions
**************

Annotation file
====================

- Create an annotation file as directed `here <annotation.html>`__.


CSV file
============

- Create or export a spreadsheet file as directed `here <spreadsheet.html>`__.

