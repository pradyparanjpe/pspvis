###############
SOURCE CODE DOC
###############

************
Entry Point
************

Package import
==============

User Interface
---------------------------------

.. automodule:: pspvis.front_ui
   :members:

=============================================================================

*******
Errors
*******

Error/Warnings
==============

.. automodule:: pspvis.errors
   :members:

=============================================================================

**************
Backend
**************

Annotation
============

.. automodule:: pspvis.annotation

Plot
======

.. automodule:: pspvis.plot
   :members:
