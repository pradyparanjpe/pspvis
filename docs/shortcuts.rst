###########################
Shortcuts
###########################

**************************
User Interface Shortcuts
**************************

Global
==============

Ctrl-
---------

Following shortcuts are triggered by pressing the mentioned key while holding **Ctrl** key

- **o**: Load Spreadsheet
- **Shift + O**: Load annotations
- **w**: Throw away current spreadsheet
- **h**: Open Help documentation
- **=**: Increase spot-size by 10 units (units are "points^2")
- **+**: Increase spot-size by 10 units
- **-**: Reduce spot-size by 10 units
- **0**: Reset spot-size to 50 units
- **Shift + H**: Toggle spot-hold-mode
- **f**: Focus on search field
- **m**: Clear annotation marks


Unmodified
------------

Following shortcuts are triggered by pressing the mentioned keys:

- **Return**: Launch Search for whatever is entered in the search field.
- **F5**: Refresh plot
