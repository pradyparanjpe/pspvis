#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2020-2021 Pradyumna Paranjape
#
# This file is part of pspvis.
#
# pspvis is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pspvis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details. #
# You should have received a copy of the GNU Lesser General Public License
# along with pspvis. If not, see <https://www.gnu.org/licenses/>.
#
"""
structure classes
"""

import configparser
from pathlib import Path
from types import GeneratorType
from typing import Dict, List, Mapping, Optional, Tuple, Union

import toml
import yaml

from pspvis.errors import VisKeyError


def _load_yaml(config: Path) -> Dict[str, dict]:
    """
    Read configuration specified as a yaml file:
        - *.yml
        - *.yaml
    """
    with open(config, 'r') as rcfile:
        conf: Dict[str, dict] = yaml.safe_load(rcfile)
    if conf is None:  # pragma: no cover
        raise yaml.YAMLError
    return conf


def _load_ini(config: Path) -> Dict[str, dict]:
    """
    Read configuration supplied in
        - *.cfg
        - *.conf
    """
    parser = configparser.ConfigParser()
    parser.read(config)
    return {
        datacfg: dict(parser.items(datacfg))
        for datacfg in parser.sections()
    }  # pragma: no cover


def _load_toml(config: Path) -> Dict[str, dict]:
    """
    Read configuration supplied in ``pyproject.toml`` OR
        - *.toml
    """
    with open(config, 'r') as rcfile:
        conf: Dict[str, dict] = dict(toml.load(rcfile))
    if conf is None:  # pragma: no cover
        raise toml.TomlDecodeError
    return conf


class DataAnnot():
    """
    Data Annotation structure class

    Attributes:
        rows: Names of rows with annotation
        cols: Names of columns with annotation
        schemes: collection schemes, each scheme groups rows in a way

    Args:
        annot: information annot file

    """
    def __init__(self, annot: Union[Mapping, Path] = None):
        self._rows: Dict[str, str] = {}
        self._cols: Dict[str, str] = {}
        self.schemes: Dict[str, Dict[str, List[str]]] = {'None': {}}

        if annot:
            if isinstance(annot, Mapping):
                self.fill_data(annot)
            else:
                # assume Path
                self.load_annotation(annot)

    @property
    def cols(self) -> Dict[str, str]:
        return self._cols

    @cols.setter
    def cols(self, val: Dict[str, str]):
        self._cols = val
        self.schemes['None'].update({acc: [acc] for acc in self._cols})

    @cols.deleter
    def cols(self):
        self._cols = {}

    @property
    def rows(self) -> Dict[str, str]:
        return self._rows

    @rows.setter
    def rows(self, val: Dict[str, str]):
        self._rows = val
        self.schemes['None'].update({acc: [acc] for acc in self._rows})

    @rows.deleter
    def rows(self):
        self._rows = {}

    def __bool__(self) -> bool:
        """
        if DataAnnot
        """
        if self._rows or self._cols:
            return True
        return False

    def __repr__(self) -> str:
        """
        Represent data
        """
        output: List[str] = ['Scope:']
        output.extend(('  ' + acc for acc in self.rows))
        output.append('Scheme')
        for name, scheme in self.schemes.items():
            output.append('  ' + name)
            for grp, acc_l in scheme.items():
                output.append('  ' * 2 + str(grp))
                for sr_no, acc in enumerate(acc_l):
                    output.append('  ' * 3 + str(sr_no) + ': ' + acc)
        return '\n'.join(output)

    def __getitem__(
        self, args: Union[str, Tuple[str, Union[str, int]]]
    ) -> Union[List[str], List[Union[str, int]]]:
        """
        Args:
            args: if args is single argument
                    accession is assumed, annotation is returned

                else, args is to be a tuple of following args
                    scheme: scheme in use
                    group_name: return all elements of group
                    accession [row/col]: return all groups that contain the row
        """
        if not isinstance(args, (list, tuple)):
            if args in self.rows:
                return [self.rows[args]]
            if args in self.cols:
                return [self.cols[args]]
            raise VisKeyError(f'Accession key {args} is unknown]')
        scheme, key = args
        if scheme not in self.schemes:
            raise VisKeyError(f'Bad index {scheme}')
        group_scheme = self.schemes[scheme]
        if key in (*self.rows, *self.cols):
            # key is a known element
            return list(gid for gid, group in group_scheme.items()
                        if key in group)
        if key in group_scheme:
            return group_scheme[str(key)]
        raise VisKeyError(f'Bad index {key}')

    def __contains__(self, key: str) -> bool:
        """
        Key is a known row or column index
        But NOT scheme or group

            * for scheme, use self.schemes.__contains__
            * for group, use self.schemes[scheme_idx].__contains__
        """
        if key in self.rows:
            return True
        if key in self.cols:
            return True
        return False

    def get(
        self,
        args: Union[str, Tuple[str, Union[str, int]]],
        default: Optional[Union[List[str], List[Union[str, int]]]] = None
    ) -> Optional[Union[List[str], List[Union[str, int]]]]:
        try:
            return self.__getitem__(args)
        except VisKeyError:
            return default

    def update(self, template: 'DataAnnot') -> None:
        """
        Update from template
        Args:
            template: object to immitate
        """
        # Update scope
        self.rows.update(template.rows)
        self.cols.update(template.cols)

        # update groups
        for name, data in template.schemes.items():
            if name not in self.schemes:
                self.schemes[name] = data
            else:
                # the following line will **replace** accession list,
                # which is the intended behaviour
                self.schemes[name].update(data)

    def load_annotation(self, config: Path) -> None:
        """
        load configuration file

        Args:
            conf: configuration file path (yaml, toml or ini file)
        """
        if config.suffix in ('.yaml', '.yml'):
            conf = _load_yaml(config)
        elif config.suffix == 'toml':
            conf = _load_toml(config)
        elif config.suffix in ('.conf', '.cfg'):
            conf = _load_ini(config)
        else:
            # try each
            try:
                conf = _load_yaml(config)
            except yaml.YAMLError:
                try:
                    conf = _load_toml(config)
                except toml.TomlDecodeError:
                    conf = _load_ini(config)
        self.fill_data(conf)

    def fill_data(self, annot: Mapping) -> None:
        """
        Parse annot and create structure
        """
        config_rows: Union[List[str], Dict[str, str]] = annot.get('ROWS', {})
        config_cols: Union[List[str], Dict[str,
                                           str]] = annot.get('COLUMNS', {})
        scheme_dict: Dict[str, List[str]]
        if isinstance(config_rows, list):
            config_rows = {acc: '' for acc in config_rows}
        if isinstance(config_cols, list):
            config_cols = {acc: '' for acc in config_cols}
        self.rows.update(config_rows)
        self.cols.update(config_cols)
        for name, data in annot.get('SCHEMES', {}).items():
            if name not in self.schemes:
                assert isinstance(name, str)
                self.schemes[name] = {}
            if isinstance(data, GeneratorType):
                scheme_dict = {str(idx): grp for idx, grp in enumerate(data)}
            else:
                try:
                    # can we easily convert data into a dict?
                    scheme_dict = {
                        str(idx): grp
                        for idx, grp in dict(data).items()
                    }
                except ValueError:
                    scheme_dict = {
                        str(idx): grp
                        for idx, grp in enumerate(data)
                    }
            for grp, acc_list in scheme_dict.items():
                assert isinstance(grp, (int, str))
                assert isinstance(acc_list, list)
            self.schemes[name].update(scheme_dict)

    def write(self, filename: Path, ftype: str = 'guess') -> None:
        """
        Write to file.

        Args:
            filename: path to file
            ftype: {yml,toml,ini} file type
        """
        # validate filetype
        ftype = ftype.strip('.')
        if ftype == 'guess':
            if filename.suffix == '.toml':
                ftype = 'toml'
            elif filename.suffix in ('.conf', '.ini', '.cfg'):
                ftype = 'ini'
            else:
                ftype = 'yml'
        ftype = {'yaml': 'yml', 'conf': 'ini', 'cfg': 'ini'}.get(ftype, ftype)
        if ftype not in ('yml', 'toml', 'ini'):
            raise ValueError(f'Bad extension: {ftype}')

        # compile data
        data = {
            'ROWS': self.rows,
            'COLUMNS': self.cols,
            'SCHEMES': {
                sid: scheme
                for sid, scheme in self.schemes.items() if sid != 'None'
            }
        }

        # write
        if ftype == 'ini':
            parser = configparser.ConfigParser()
            for key, value in data.items():
                parser[key] = value
            with open(filename, 'w') as config_write:
                parser.write(config_write)
        elif ftype == 'toml':
            with open(filename, 'w') as config_write:
                toml.dump(data, config_write)
        elif ftype == 'yml':
            with open(filename, 'w') as config_write:
                yaml.dump(data, config_write)
