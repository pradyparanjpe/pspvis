#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2020-2021 Pradyumna Paranjape
#
# This file is part of pspvis.
#
# pspvis is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pspvis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pspvis. If not, see <https://www.gnu.org/licenses/>.
#
"""
preferences
"""

from pathlib import Path
from tkinter import ttk
from typing import Dict

import yaml
from matplotlib import pyplot as plt
from ttkthemes import ThemedTk
from xdgpspconf import read_config, safe_config

from pspvis.data_types import PlotUserVars


def read_pref(custom: Path = None) -> PlotUserVars:
    """
    Read Preferences from standard xdg locations.

    Args:
        custom: custom preference file path

    Returns:
        parsed else empty preference object

    """
    # initialize with config
    conf = PlotUserVars()
    config_preferences = read_config('pspvis', custom, False, 'preferences')
    for config in config_preferences.values():
        try:
            conf.update(config)
        except Exception:
            pass
    return conf


def write_config(conf: PlotUserVars):
    """
    write configuration to standard location in yaml format
    """
    pref_paths = safe_config('pspvis', '.yml', False, 'preferences')
    for pref_loc in pref_paths:
        try:
            pref_loc.parent.mkdir(parents=True, exist_ok=True)
            with open(pref_loc, 'w') as pref_out:
                yaml.dump(conf.__dict__, pref_out)
                break
        except (PermissionError, IsADirectoryError, FileNotFoundError):
            pass


def collect_themes() -> Dict[str, Dict[str, str]]:
    """
    Collect avail_themes from 'avail_themes' folder and mpl's API
    """
    avail_themes: Dict[str, Dict[str, str]]
    avail_themes = {'ui': {'default': ''}, 'mpl': {'default': 'classic'}}

    temproot = ThemedTk()
    style = ttk.Style(temproot)
    temproot.destroy()
    for theme in style.theme_names():
        avail_themes['ui'][theme] = theme

    for theme in plt.style.available:
        avail_themes['mpl'][theme] = theme

    return avail_themes
